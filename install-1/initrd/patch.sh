#!/bin/bash
### modify initrd so that it uses the the modified setup scripts

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)
path=$(pwd)

### mount
./mount.sh ../images/slack-cd1/isolinux/initrd.img

### patch the setup scripts
cd initrd/usr/lib/setup/
patch -p1 -ulE -i $path/setup-patch.diff
cd $path

### copy to slackserver-cd
gzip initrd.img
mv initrd.img.gz ../slackserver-cd/isolinux/initrd.img

### clean
./clean.sh 2>/dev/null
