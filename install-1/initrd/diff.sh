#!/bin/bash
### make a patch for the setup scripts of initrd

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### mount
./mount.sh ../images/slack-cd1/isolinux/initrd.img

### make the patch
diff -rubB initrd/usr/lib/setup/ slackserver-setup/ > setup-patch.diff

### clean
./clean.sh
