#!/bin/sh
### Make the ISO image of the installation CD

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### make initrd.img (if does not already exist)
if [ ! -f slackserver-cd/isolinux/initrd.img ]
then
  initrd/diff.sh
  initrd/patch.sh  
fi

### create the ISO image
mkisofs -R -J -v -d -N -hide-rr-moved \
        -no-emul-boot -boot-load-size 4 -boot-info-table \
        -sort images/slack-cd1/isolinux/iso.sort \
        -b isolinux/isolinux.bin \
        -c isolinux/isolinux.boot \
        -V "SlackServer Install" \
        -A "SlackServer Install CD" \
        -o images/slackserver-install.iso \
        -exclude-list exclude-list.txt \
        images/slack-cd1/ \
        slackserver-cd/ 

