#!/bin/bash
### Modifies the packages that are installed by default
### by modifying the files 'tagfile', 'maketag' and 'maketag.ez'.

### go to this directory
cd $(dirname $0)
path=$(pwd)

### get a copy of unmodified tagfile-s
cd images/slack-cd1/slackware/
find . -name tagfile -o -name maketag -o -name maketag.ez \
  | xargs tar cfz $path/tags.tgz
cd $path/slackserver-cd/slackware/
tar xfz $path/tags.tgz
cd $path
rm tags.tgz
rm -rf $path/slackserver-cd/slackware/{x,xap,y}

### mark all packages in slackserver-cd by SKP and "off"
dir=slackserver-cd/slackware/
find $dir -name tagfile | xargs sed -i -e "s/: ADD/: SKP/"
find $dir -name tagfile | xargs sed -i -e "s/: REC/: SKP/"
find $dir -name tagfile | xargs sed -i -e "s/: OPT/: SKP/"
find $dir -name 'maketag' -o -name 'maketag.ez' \
     | xargs sed -i -e 's/ "on" / "off" /'

### get a list of packages that must be installed
package_list=$(cat packages.txt)

### mark the packages in the list by ADD
for pkg in $package_list
do
  pkgname=$(echo $pkg | sed -e "s/-[0-9].*//")
  ### get the disk (path) of the package and the package name
  path=$(find images/slack-cd1/slackware/ -name $pkgname-[0-9]*.tgz)
  path=$(basename $(dirname $path))
  # echo $path -- $pkgname -- $pkg ## debug

  ### mark this package by ADD in the tagfile of slackserver-cd
  sed -i -e "/^$pkgname/ s/: SKP/: ADD/" slackserver-cd/slackware/$path/tagfile
  ### mark it as "on" in 'maketag' and 'maketag.ez' of slackserver-cd
  sed -i -e "/^\"$pkgname\"/ s/ \"off\" / \"on\" /" \
      slackserver-cd/slackware/$path/maketag \
      slackserver-cd/slackware/$path/maketag.ez
done

### the "s" series (custom server packages) should not be touched
svn revert -R slackserver-cd/slackware/s/
