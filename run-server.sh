#!/bin/bash
### run qemu with the given system image

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### get the system image
sysimg=${1:-images/slackserver.img}

### start qemu
qemu -kernel-kqemu -m 128 $sysimg -redir \
     tcp:88:10.0.2.15:80 -redir tcp:222:10.0.2.15:22
