#!/bin/bash
### Make a backup archive (.tar.gz) of /dev/hda2 of 'images/slackserver.img' 
### to the file 'images/slackserver.hda2.tar.gz'. This can be used then
### by 'slackserver-install' to make a new copy of the system.

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### mount /dev/hda2 of slackserver.img to the directory images/tmp/
/sbin/losetup -o 271434240 /dev/loop0 images/slackserver.img
mkdir -p images/tmp/
mount /dev/loop0 images/tmp/

### create the backup archive
tar --create --gzip --preserve --directory=images/tmp/ \
    --file=images/slackserver.hda2.tar.gz .

### clean up
umount images/tmp/
/sbin/losetup -d /dev/loop0

