#!/bin/sh
### Make the ISO image of the easy installation CD.
### This CD is a Slackware bootable CD, without packages,
### containing instead the backup archive of the server system
### and the installation script 'slackserver-install'.

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### make initrd.img
./make-initrd.sh

### create the ISO image
mkisofs -R -J -v -d -N -hide-rr-moved -graft-points \
        -no-emul-boot -boot-load-size 4 -boot-info-table \
        -sort images/slack-cd1/isolinux/iso.sort \
        -b isolinux/isolinux.bin \
        -c isolinux/isolinux.boot \
        -V "Slackserver Easy Install" \
        -A "Slackserver Easy Install CD" \
        -o images/slackserver-easy-install.iso \
        -x images/slack-cd1/isolinux/initrd.img \
        -x images/slack-cd1/slackware \
        images/slack-cd1/ \
        /isolinux/=initrd.img \
        /=images/slackserver.hda2.tar.gz \
        /=slackserver-install

### clean
rm initrd.img

