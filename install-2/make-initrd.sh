#!/bin/bash
### modify initrd by adding the script slackserver-install

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### copy initrd.img, unzip and mount it
cp images/slack-cd1/isolinux/initrd.img initrd.img.gz
gunzip initrd.img.gz
mkdir initrd
mount -o loop initrd.img initrd

### modify initrd
cp slackserver-install initrd/
chown root.root initrd/slackserver-install

### copy to distrib-cd
umount initrd/
gzip initrd.img
mv initrd.img.gz initrd.img

### clean
rmdir initrd

