
There are three ways for installing the slackserver. The first one is
to do everything manually from the scratch. To do it in an emulated
machine (using qemu), run the script 'install-from-cd.sh'. Then
install slackware from its installation CDs, trying to make it as
small as possible (installing as few packages as possible, but trying
to include the required ones). Finally configure it properly and test
it untill it is OK.

The second way is to modify the slackware first installation CD and to
create a custom installation CD for slackserver. This is done with the
help of the scripts in 'install-1/'. A list of the installed packages
(from the manual installation above) is used as well. The main
advantage of this installation CD over the slackware installation CD
is that the right packages to be installed are selected by default,
and no time should be spent to select them, to solve dependencies,
etc. This installation CD preserves also the flexibility of the
original slackware CD (so that additional slack packages can be
installed in case that the server is going to be used for other
purposes as well). On the other hand, not everything can be done
automatically and it still needs manual configuration in order to make
everything work.

The third way is by making an archive of an installed and configured
system (prepared with one of the ways described above), writing it on
a boot CD and from the CD copying it to a new blank harddisk. So the
new system will be an almost identical copy of the original one. This
reduces the flexibility of the installation, however it has the
advantage of the simplicity, and the installation can be done even by
non-technical people. The scripts that help to make such an
installation are in the directory 'install-2/'.

The directory 'images/' contains the images of the emulated systems or
installation CDs and usually needs a lot of space. It can also be a
(symbolic) link to another partition that has enough free space.

The script 'write-iso-to-cd.sh' is used to write an iso to a blank CD.
It takes as parameter the filename of the iso to be written.

The script 'install-from-cd.sh' emulates a system which boots from an
installation CD (iso). The first parameter is the installation
CD/iso. It can be 'images/slack-cd1.iso', '/dev/hdc',
'images/slackserver-install.iso', etc. Default is
'images/slack-cd1.iso'. The second parameter is the name of the disk
image where the installation will be done. The default value is
'images/slackserver.img'. The disk image will be created, if it does
not exist, and the third parameter is the size of the disk image to be
created. The default size is 1GB.

The script 'run-server.sh' starts the emulation of the server image
that is passed to it as the first parameter. The default system image
(if no parameter is given) is 'images/slackserver.img'. The web server
of the emulated system can be accessed at 127.0.0.1:88, and the shell
(ssh) can be accessed at 127.0.0.1:222.

The file 'qemu.txt' explains some things about the installation and
usage of qemu.
