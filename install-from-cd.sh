#!/bin/bash
### Emulate a system which boots from an installation CD (iso).
### The first parameter is the installation CD/iso. It can be
### images/slack-cd1.iso, /dev/hdc, images/slackserver-install.iso, etc.
### Default is images/slack-cd1.iso.
### The second parameter is the name of the disk image where the
### installation will be done. The default value is 'images/slackserver.img'.
### The disk image will be created, if it does not exist, and the
### third parameter is the size of the disk image to be created.
### The default size is 1GB.

### go to this directory
cd $(dirname $0)

### get parameters
cdrom=${1:-images/slack-cd1.iso}
diskimg=${2:-images/slackserver.img}
size=${3:-1000MB}

### create the image if it doesn't exist
if [ ! -f $diskimg ]; then qemu-img create $diskimg $size; fi

### install netaccess from the image
qemu -kernel-kqemu -boot d -cdrom $cdrom $diskimg

### testing the installed system
echo "To test the installed system run the command:"
echo "./start-system.sh $image"

